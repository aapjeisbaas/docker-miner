#!/bin/bash

WORKER_NAME="${WORKER_NAME:=${HOSTNAME}}"
echo "Using ${WORKER_NAME} as the worker name"

# defaults for testing and supporting development
WALLET="${WALLET:=0xe589531e35fa84d9d561f028070663004aca5fa4}"
POOL="${POOL:=eth-eu1.nanopool.org:9433}"
EXTRA="${EXTRA:=--ssl 1}"

# FIX NVML error
FILE="/lib/x86_64-linux-gnu/libnvidia-ml.so"
if [ -f "$FILE" ]; then
    echo "$FILE exists, no fix needed."
else 
    echo "$FILE does not exist, creating symlink."
    ln -s /lib/x86_64-linux-gnu/libnvidia-ml.so.1 /lib/x86_64-linux-gnu/libnvidia-ml.so
fi


/app/Gminer --opencl 0 --algo ethash --server ${POOL} --user ${WALLET}.${WORKER_NAME}  ${EXTRA}&
PID=$!

# gracefull shutdown handler
stop_handler () {
    echo "Caught an exit signal stoppping gracefully"
    kill -s SIGINT $PID
}

# trap the incomming signals and trigger stop_handler 
trap stop_handler SIGTERM SIGKILL SIGINT

# wait for software to exit
wait $PID
