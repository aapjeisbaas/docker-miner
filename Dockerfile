FROM nvidia/cuda:11.2.1-base

RUN apt update && apt install -y --no-install-recommends libpci3 \
  && apt-get clean autoremove \
  && rm -rf /var/lib/apt/lists/*

COPY ["./start.sh", "./miners/Gminer", "/app/"]

CMD ["/app/start.sh"]
