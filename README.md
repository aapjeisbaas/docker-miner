
This repo is used to create a nvidia cuda enabled container to run [Phoenix Miner](https://phoenixminer.org/)

I run this container on kubernetes I followed this guide to set it up: [install k8s ubuntu-lts](https://docs.nvidia.com/datacenter/cloud-native/kubernetes/install-k8s.html#ubuntu-lts)

A more minimal just nvidia-docker setup is also available: [install nvidia-docker2](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html#installing-on-ubuntu-and-debian)


## Running this container
```
# minimal docker run command
docker run --gpus all --env EXTRA='-gt 0' --env WORKER_NAME='worker-name' --env WALLET='0x.....' --env POOL='ssl://eth-eu1.nanopool.org:9433' aapjeisbaas/docker-miner
```

## deploy on your nvidia kubernetes
```
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: miner-deployment
  namespace: default
spec:
  replicas: 1
  selector:
    matchLabels:
      app: miner
  strategy: 
     type: Recreate
  template:
    metadata:
      labels:
        app: miner
    spec:
      containers:
        - name: miner
          image: aapjeisbaas/docker-miner:latest
          env:
          - name: "EXTRA"
            value: "-gt 0"
          resources:
            requests:
              memory: "50Mi"
              cpu: "20m"
            limits:
              memory: "500Mi"
              cpu: "500m"
          imagePullPolicy: Always
# if you have multiple nodes you can use this selector to pin your miner
#     nodeSelector:
#       kubernetes.io/hostname: nvidia-enabled-hostname
```